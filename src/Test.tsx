import { useState } from 'react'
import tt from './indes'
import styles from './test.module.scss'

function Test() {
  const [count, setCount] = useState(10)
  // console.log(styles);

  return (
    <>
      <div className="card">
        <button type="button" onClick={() => setCount(prev => prev + 1)} className={styles.rootCest}>
          count is {count}
        </button>
        <button type="button" onClick={() => setCount(0)}>
          reset
        </button>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className="read-the-docs">Click on the Vite and React logos to learn more {count}</p>
      <p className="read-the-docs">{tt}</p>
    </>
  )
}

export default Test
