import reactLogo from './assets/react.svg'
import './App.css'
import Test from './Test'

function App() {
  // const test = '123'
  // const test2 = '123'
  // const test3 = '123'
  // const test4 = '123'
  // console.log(123)
  // console.log(123)

  return (
    <div className="App">
      <div>
        <a href="https://vitejs.dev" target="_blank" rel="noreferrer">
          <img src="/vite.svg" className="logo" alt="Vite logo" />
        </a>
        <a href="https://reactjs.org" target="_blank" rel="noreferrer">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Vite + React</h1>

      <Test />
    </div>
  )
}

export default App
